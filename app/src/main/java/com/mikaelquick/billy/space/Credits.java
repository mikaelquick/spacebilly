package com.mikaelquick.billy.space;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class Credits extends AppCompatActivity {

    Typeface starwarsFont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_credits);

        TextView creditsLogo = (TextView)findViewById(R.id.creditsLogo);
        TextView createdBy = (TextView)findViewById(R.id.createdBy);
        TextView specialThanks = (TextView)findViewById(R.id.specialThanks);
        TextView egoOlsen = (TextView)findViewById(R.id.egonOlsen);
        TextView egoInfo = (TextView)findViewById(R.id.egoInfo);
        TextView andText = (TextView)findViewById(R.id.and);
        TextView iluenVtx = (TextView)findViewById(R.id.iluen);

        starwarsFont = Typeface.createFromAsset(getAssets(), "starwars.ttf");
        creditsLogo.setTypeface(starwarsFont);
        createdBy.setTypeface(starwarsFont);
        specialThanks.setTypeface(starwarsFont);
        egoOlsen.setTypeface(starwarsFont);
        egoInfo.setTypeface(starwarsFont);
        andText.setTypeface(starwarsFont);
        iluenVtx.setTypeface(starwarsFont);


    }
}
