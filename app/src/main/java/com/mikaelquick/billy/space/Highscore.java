package com.mikaelquick.billy.space;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Highscore extends AppCompatActivity {

    //Highscore
    int firstplace;
    int secondplace;
    String firstplaceName;
    String secondplaceName;

    //Texviews
    TextView textViewGold;
    TextView textViewSilver;

    //Others
    FirebaseDatabase database;
    Typeface starwarsFont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Init
        database = FirebaseDatabase.getInstance();
        final DatabaseReference thehighscore = database.getReference("Highscore");

        //Fullscreen
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_highscore);

        //Getting the textview & buttons
        textViewGold = (TextView) findViewById(R.id.texviewGold);
        textViewSilver = (TextView) findViewById(R.id.textViewSilver);
        TextView highscoreLogo = (TextView) findViewById(R.id.highscoreLogo);

        textViewGold.setText("Loading" + "\n" + "Score");
        textViewSilver.setText("Loading" + "\n" + "Score");

        //Database
        AdView mAdView = (AdView) findViewById(R.id.adViewHighscore);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .build();
        mAdView.loadAd(adRequest);

        thehighscore.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                firstplace = Integer.parseInt(dataSnapshot.child("Firstplace").child("Score").getValue().toString());
                firstplaceName = dataSnapshot.child("Firstplace").child("Name").getValue().toString();

                secondplace = Integer.parseInt(dataSnapshot.child("Secondplace").child("Score").getValue().toString());
                secondplaceName = dataSnapshot.child("Secondplace").child("Name").getValue().toString();

                textViewGold.setText(firstplaceName + "\n" + firstplace);
                textViewSilver.setText(secondplaceName + "\n" + secondplace);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //Adding the font
        starwarsFont = Typeface.createFromAsset(getAssets(), "starwars.ttf");
        highscoreLogo.setTypeface(starwarsFont);
    }

    @Override
    protected void onResume() {
        database.goOnline();
        super.onResume();
    }

    @Override
    protected void onPause() {

        database.goOffline();
        super.onPause();
    }

}

