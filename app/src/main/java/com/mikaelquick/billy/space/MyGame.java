package com.mikaelquick.billy.space;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.threed.jpct.Camera;
import com.threed.jpct.CollisionEvent;
import com.threed.jpct.CollisionListener;
import com.threed.jpct.Config;
import com.threed.jpct.FrameBuffer;
import com.threed.jpct.Object3D;
import com.threed.jpct.SimpleVector;
import com.threed.jpct.Texture;
import com.threed.jpct.TextureManager;
import com.threed.jpct.World;
import com.threed.jpct.util.BitmapHelper;
import com.threed.jpct.util.SkyBox;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class MyGame extends Activity implements GLSurfaceView.Renderer, View.OnTouchListener, CollisionListener {

    //Objects
    MyModels myModels;
    MySpaceship mySpaceship;

    //Object3d
    private Object3D spaceship;
    Object3D made;

    //Vectors
    private SimpleVector move;
    private SimpleVector ellipsoid;

    //Highscore Values
    int firstplace;
    int secondplace;
    String firstplaceName;
    String placementMessage;
    boolean thescore = false;

    //Database
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    final DatabaseReference thehighscore = database.getReference("Highscore");
    final FirebaseAuth mAuth = FirebaseAuth.getInstance();

    //Opengl
    private GLSurfaceView glView;
    private World world;
    private FrameBuffer fb;
    Camera c;
    SkyBox skybox;

    //Booleans
    boolean gameISrunning = false;
    boolean startTimer = true;
    boolean moving = false;
    boolean gotHighscore;

    //Textviews & others
    TextView textviewPickups;
    TextView textviewTime;
    EditText editText;
    ImageView loadingScreen;
    ImageView joystickPicture;
    ProgressBar bar;

    //Values touch
    private float leftOfSpaceShip;
    private float leftierOfSpaceship;
    private float leftiestOfSpaceship;
    private float rightierOfSpaceship;
    private float rightOfSpaceShip;
    private float rightestOfSpaceship;
    private float mostDownSpaceShip;
    private float mostUpSpaceShip;

    //Device screensize
    private int mobileWidth;
    private float mobileHeight;

    //Others
    CountDownTimer countDownTimer;
    private int amountOfPicups = 0;
    private float lastX, lastY;
    Texture joystick;



    private int px(float dips) {
        float DP = getResources().getDisplayMetrics().density;
        return Math.round(dips * DP);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        TextureManager.getInstance().flush();

        //Setting up the surface
        glView = new GLSurfaceView(this);
        glView.setEGLContextClientVersion(2);
        glView.setRenderer(this);

        //Getting the objects
        myModels = new MyModels();
        mySpaceship = new MySpaceship();

        //Initierar
        ellipsoid = new SimpleVector(20, 20, 20);
        move = new SimpleVector(0, 0, 0);
        bar = new ProgressBar(this);
        textviewPickups = new TextView(this);
        textviewTime = new TextView(this);
        editText = new EditText(this);
        loadingScreen = new ImageView(this);
        joystickPicture = new ImageView(this);
        joystickPicture.setBackground(this.getResources().getDrawable(R.drawable.joystick));
        loadingScreen.setBackground(this.getResources().getDrawable(R.drawable.loading));

        mobileWidth = getWindowManager().getDefaultDisplay().getWidth();
        mobileHeight = getWindowManager().getDefaultDisplay().getHeight();


        //Creating a suiting layout & adding the views
        RelativeLayout rel = new RelativeLayout(this);
        rel.addView(glView);
        rel.addView(textviewTime);
        rel.addView(textviewPickups);
        rel.addView(loadingScreen);
        rel.addView(joystickPicture);
        rel.addView(bar);
        joystickPicture.getLayoutParams().height = (int) Math.round(mobileHeight / 2.25);
        joystickPicture.getLayoutParams().width = (int) Math.round(mobileWidth / 3.7);
        joystickPicture.setVisibility(View.INVISIBLE);

        //Setting up the textviews
        textviewPickups.setVisibility(View.GONE);
        textviewPickups.setTextColor(Color.WHITE);
        textviewPickups.setTextSize(40);
        textviewPickups.setText("0");

        textviewTime.setTextColor(Color.WHITE);
        textviewTime.setTextSize(40);

        //Fullscreen
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //Setting up rules for objects in the layout
        RelativeLayout.LayoutParams loadingBarRule = (RelativeLayout.LayoutParams) bar.getLayoutParams();
        loadingBarRule.addRule(RelativeLayout.CENTER_IN_PARENT);
        bar.setLayoutParams(loadingBarRule);

        RelativeLayout.LayoutParams loadingScreenRule = (RelativeLayout.LayoutParams) loadingScreen.getLayoutParams();
        loadingScreenRule.addRule(RelativeLayout.CENTER_IN_PARENT);

        RelativeLayout.LayoutParams timeRule = (RelativeLayout.LayoutParams) textviewTime.getLayoutParams();
        timeRule.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

        RelativeLayout.LayoutParams joystickRule = (RelativeLayout.LayoutParams) joystickPicture.getLayoutParams();
        joystickRule.setMargins(px(15), 0, 0, px(5));
        joystickRule.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

        //Displaying the mainview
        setContentView(rel);

        //Receiving the height & width of the device
        Texture joystickTexture = new Texture(
                BitmapHelper.rescale(BitmapHelper.convert(this.getResources().getDrawable(R.drawable.joystick)), 256, 256));
        TextureManager.getInstance().addTexture("joystick", joystickTexture);

        joystick = TextureManager.getInstance().getTexture("joystick");

        //Determing if the screen is touched left/right/up/down of the spaceship

        leftOfSpaceShip = mobileWidth / 8;
        rightOfSpaceShip = mobileWidth / 5.6f;

        leftierOfSpaceship = mobileWidth / 9.35064935f;
        leftiestOfSpaceship = mobileWidth / 18.4615384615f;

        rightierOfSpaceship = mobileWidth / 4.53900709f;
        rightestOfSpaceship = mobileWidth / 4;

        mostUpSpaceShip = mobileHeight / 1.39f;
        mostDownSpaceShip = mobileHeight / 1.24612108036f;


        createCounter();
        glView.setOnTouchListener(this);

        thehighscore.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                secondplace = Integer.parseInt(dataSnapshot.child("Secondplace").child("Score").getValue().toString());
                firstplace = Integer.parseInt(dataSnapshot.child("Firstplace").child("Score").getValue().toString());
                firstplaceName = dataSnapshot.child("Firstplace").child("Name").getValue().toString();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });
    }

    @Override
    protected void onResume() {
        glView.onResume();
        database.goOnline();
        super.onResume();

    }

    @Override
    protected void onPause() {
        glView.onPause();
        database.goOffline();
        super.onPause();

        if (!gameISrunning) {
            startActivity(new Intent(this, Startmenu.class));
            finish();
        }
    }

    @Override
    public void onSurfaceCreated(GL10 gl10, EGLConfig eglConfig) {
        bar.setVisibility(ProgressBar.VISIBLE);
        new Thread() {

            public void run() {
                loadingGameo();
                gameISrunning = true;

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        bar.setVisibility(ProgressBar.INVISIBLE);
                        loadingScreen.setVisibility(View.GONE);
                        joystickPicture.setVisibility(View.VISIBLE);
                    }
                });
            }
        }.start();
    }

    public void loadingGameo() {
        Config.farPlane = 8000;
        Config.defaultCameraFOV = 1f;

        //setting up the world with all of its components
        world = new World();
        skybox = myModels.getSkybox(this);
        world.setAmbientLight(200, 200, 200);

        //creating the objects in the world
        mySpaceship.createSpaceship(world);
        spaceship = mySpaceship.getSpaceship();
        myModels.createPickup(world, this, 40, this);
        myModels.createMade(this, world, 100);
        myModels.createMerc(this, world);
        made = myModels.getMadeModel();
        myModels.createEris(this, world);
        mySpaceship.getGlass().setCollisionMode(Object3D.COLLISION_CHECK_SELF);
        spaceship.setCollisionMode(Object3D.COLLISION_CHECK_SELF);

    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        fb = new FrameBuffer(width, height);
        mobileWidth = getWindowManager(
        ).getDefaultDisplay().getWidth();
        mobileHeight = getWindowManager().getDefaultDisplay().getHeight();
    }

    @Override
    public void onDrawFrame(GL10 gl10) {

        if (gameISrunning) {
            myModels.getMadeModel().rotateY(0.0009f);
            myModels.getMercModel().rotateY(0.0009f);
            myModels.getErisModel().rotateY(0.0009f);

            fb.clear();
            c = world.getCamera();
            move = new SimpleVector(c.getDirection().x * 2, c.getDirection().y * 2, c.getDirection().z * 2);
            skybox.render(world, fb);
            world.renderScene(fb);
            world.draw(fb);
            fb.display();
            Utils.cameraFollow(spaceship, c, world, 0, -180);

            if (moving) {
                if (startTimer) {
                    countDownTimer.start();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            textviewPickups.setVisibility(View.VISIBLE);
                        }
                    });
                    startTimer = false;
                }

                //Camera
                SimpleVector trsn = spaceship.checkForCollisionEllipsoid(move,
                        ellipsoid, 5);

                spaceship.translate(trsn);
                Utils.cameraFollow(spaceship, c, world, 0, -180);

                //Rotation
                spaceship.rotateY(mySpaceship.getxRot());
                spaceship.rotateAxis(spaceship.getXAxis(), mySpaceship.getyRot());

                //Down
                if (lastY > mostDownSpaceShip) {
                    mySpaceship.turnDown();
                }


                //Up
                if (lastY < mostUpSpaceShip) {
                    mySpaceship.turnUp();
                }

                //Middle
                if (lastY > mostUpSpaceShip && lastY < mostDownSpaceShip) {
                    mySpaceship.setxRot(0);
                    mySpaceship.setyRot(0);
                }

                //Left
                if (lastX < leftOfSpaceShip && lastX > leftierOfSpaceship) {
                    //mySpaceship.turnLeft();
                    mySpaceship.turnLeftSlow();
                }

                if (lastX < leftierOfSpaceship && lastX > leftiestOfSpaceship) {
                    mySpaceship.turnLeft();
                }

                if (lastX < leftiestOfSpaceship) {
                    mySpaceship.turnLeftFast();
                }


                //Right
                if (lastX > rightOfSpaceShip && lastX < rightierOfSpaceship) {
                    mySpaceship.turnRightSlow();
                }

                if (lastX > rightierOfSpaceship && lastX < rightestOfSpaceship) {
                    mySpaceship.turnRight();
                }

                if (lastX > rightestOfSpaceship) {
                    mySpaceship.turnRightFast();
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        TextureManager.getInstance().flush();
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {

            case MotionEvent.ACTION_DOWN:
                moving = true;
                lastX = motionEvent.getX();
                lastY = motionEvent.getY();
                return true;

            case MotionEvent.ACTION_MOVE:
                lastX = motionEvent.getX();
                lastY = motionEvent.getY();
                return true;

            case MotionEvent.ACTION_UP:
                moving = false;
                return true;
        }
        return false;
    }


    @Override
    public void collision(CollisionEvent collisionEvent) {

        if (collisionEvent.getObject().getTextureMatrix() == myModels.matrix) {
            amountOfPicups++;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    textviewPickups.setText(amountOfPicups + "");
                }
            });
            world.removeObject(collisionEvent.getObject().getID());
        }
    }

    @Override
    public boolean requiresPolygonIDs() {
        return false;
    }


    @Override
    public void finish() {
        database.goOffline();
        super.finish();
    }

    public void createCounter() {
        countDownTimer = new CountDownTimer(30000, 1000) {
            //40000
            public void onTick(long millisUntilFinished) {
                textviewTime.setText(millisUntilFinished / 1000 + "");
            }

            public void onFinish() {
                mAuth.signInAnonymously();

                if (amountOfPicups > firstplace) {
                    placementMessage = "Firstplace";
                    thescore = true;
                } else if (amountOfPicups > secondplace) {
                    placementMessage = "Secondplace";
                    thescore = true;
                }

                if (thescore) {
                    final AlertDialog.Builder highscoreDialog = new AlertDialog.Builder(MyGame.this);
                    if (!isFinishing()) {

                        highscoreDialog.setMessage("Wow you reached " + placementMessage + ", Please enter your name").setCancelable(false).setNeutralButton("Ok", null);

                    }

                    final AlertDialog alertDialog = highscoreDialog.create();
                    alertDialog.setView(editText);
                    alertDialog.show();
                    Button okButton = alertDialog.getButton(DialogInterface.BUTTON_NEUTRAL);
                    LinearLayout parent = (LinearLayout) okButton.getParent();
                    parent.setGravity(Gravity.CENTER_HORIZONTAL);
                    View leftSpacer = parent.getChildAt(1);
                    leftSpacer.setVisibility(View.GONE);

                    alertDialog.getButton(AlertDialog.BUTTON_NEUTRAL).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if (!editText.getText().toString().isEmpty() && editText.getText().length() < 14) {

                                thehighscore.child(placementMessage).child("Score").setValue(amountOfPicups);
                                thehighscore.child(placementMessage).child("Name").setValue(editText.getText().toString());
                                alertDialog.dismiss();
                                createRetryDialog();
                            }

                            if (editText.getText().toString().isEmpty()) {
                                alertDialog.setMessage("Please enter a name");
                            }

                            if (editText.getText().length() > 14) {

                                alertDialog.setMessage("Please enter a shorter name");
                            }

                            if (placementMessage == "Firstplace") {
                                thehighscore.child("Secondplace").child("Score").setValue(firstplace);
                                thehighscore.child("Secondplace").child("Name").setValue(firstplaceName);
                            }

                        }
                    });


                } else {
                    createRetryDialog();
                }

            }

        };
        mAuth.signOut();
    }

    @Override
    public void onBackPressed() {
        if (gameISrunning) {
            createRetryDialog();
        }
    }

    void createRetryDialog() {
        if (!isFinishing()) {
            AlertDialog.Builder retryDialog = new AlertDialog.Builder(MyGame.this);
            retryDialog.setMessage("Do you want to retry?").setCancelable(false).
                    setPositiveButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            moving = false;
                            finish();

                        }
                    })
                    .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            MyGame.this.recreate();


                        }
                    });
            AlertDialog alert = retryDialog.create();

            alert.show();
        }
    }
}
