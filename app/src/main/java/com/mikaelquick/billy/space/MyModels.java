package com.mikaelquick.billy.space;

import android.content.Context;

import com.threed.jpct.CollisionListener;
import com.threed.jpct.Matrix;
import com.threed.jpct.Object3D;
import com.threed.jpct.Texture;
import com.threed.jpct.TextureManager;
import com.threed.jpct.World;
import com.threed.jpct.util.BitmapHelper;
import com.threed.jpct.util.SkyBox;

import java.util.Random;

public class MyModels {

    private boolean showTextures = true;
    private Object3D tempModell;
    private Object3D madeModel;
    private Object3D mercModel;
    private Object3D pickup;
    private Object3D erisModel;
    Matrix matrix;
    Matrix madeMatrix;

    Utils utils = new Utils();

    public Object3D getMercModel() {
        return mercModel;
    }

    public Object3D getMadeModel() {
        return madeModel;
    }

    public Object3D getErisModel() {
        return erisModel;
    }

    public void createMade(Context context, World world, float size) {
        if (showTextures) {
            Texture madeTexture = new Texture(
                    BitmapHelper.rescale(BitmapHelper.convert(context.getResources().getDrawable(R.drawable.made)), 1024, 1024));
            TextureManager.getInstance().addTexture("made", madeTexture);
        }

        madeModel = utils.loadModel("made", size);
        madeModel.build();
        madeModel.translate(0, 0, 2500);

        if (showTextures) {
            madeModel.setTexture("made");
        }
        this.madeMatrix = madeModel.getTextureMatrix();
        madeModel.setCollisionMode(Object3D.COLLISION_CHECK_OTHERS);
        world.addObject(madeModel);
    }

   public void createMerc(Context context, World world) {

        if (showTextures) {
            Texture mercTexture = new Texture(
                    BitmapHelper.rescale(BitmapHelper.convert(context.getResources().getDrawable(R.drawable.merc)), 1024, 1024));
            TextureManager.getInstance().addTexture("merc", mercTexture);
        }
        mercModel = madeModel.cloneObject();
        mercModel.translate(2500, -500, 1000);

        if (showTextures) {
            mercModel.setTexture("merc");
        }
        mercModel.setCollisionMode(Object3D.COLLISION_CHECK_OTHERS);
        mercModel.build();
        world.addObject(mercModel);
    }

    void createEris(Context context, World world){

        if(showTextures) {
            Texture venusTexture = new Texture(
                    BitmapHelper.rescale(BitmapHelper.convert(context.getResources().getDrawable(R.drawable.eris)), 1024, 1024));
            TextureManager.getInstance().addTexture("eris", venusTexture);
        }

        erisModel = madeModel.cloneObject();
        erisModel.translate(-1000,0,1000);
        if(showTextures) {
            erisModel.setTexture("eris");
        }
        erisModel.setCollisionMode(Object3D.COLLISION_CHECK_OTHERS);
        erisModel.build();
        world.addObject(erisModel);

    }

    SkyBox getSkybox(Context context) {
        TextureManager.getInstance().flush();
        Texture spaceTexture = new Texture(
                BitmapHelper.rescale(BitmapHelper.convert(context.getResources().getDrawable(R.drawable.stars3)), 1024, 1024));
        TextureManager.getInstance().addTexture("space", spaceTexture);

        SkyBox tempBox = new SkyBox("space", "space", "space", "space", "space", "space", 10000);

        return tempBox;
    }

    public void createPickup(World world, CollisionListener collisionListener, int amount, Context context) {

        Texture banan = new Texture(
                BitmapHelper.rescale(BitmapHelper.convert(context.getResources().getDrawable(R.drawable.banan)), 1024, 1024));
        TextureManager.getInstance().addTexture("banan", banan);
        pickup = utils.loadModel("banan2", 10);
        pickup.build();
        pickup.setTexture("banan");
        pickup.translate(0.1f, -80, 200);
        pickup.addCollisionListener(collisionListener);
        pickup.setCollisionMode(Object3D.COLLISION_CHECK_OTHERS);
        world.addObject(pickup);

        if (amount > 1) {

            for (int i = 0; i <= amount; i++) {
                int min = -500;
                int max = 2500;

                int yMin = -150;
                int yMax = 200;

                Random r = new Random();
                int i1 = r.nextInt(max - min + 1) + min;
                int i2 = r.nextInt(max - min + 1) + min;
                int i3 = r.nextInt(yMax - yMin + 1) + yMin;

                tempModell = pickup.cloneObject();
                tempModell.build();
                tempModell.translate(i2, i3, i1);
                tempModell.addCollisionListener(collisionListener);
                tempModell.setCollisionMode(Object3D.COLLISION_CHECK_OTHERS);
                this.matrix =  tempModell.getTextureMatrix();
                world.addObject(tempModell);
            }
        }
    }
}
