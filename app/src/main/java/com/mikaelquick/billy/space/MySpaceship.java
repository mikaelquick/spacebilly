package com.mikaelquick.billy.space;

import com.threed.jpct.Object3D;
import com.threed.jpct.RGBColor;
import com.threed.jpct.Texture;
import com.threed.jpct.TextureManager;
import com.threed.jpct.World;

/**
 * Created by mejk on 2017-02-27.
 */

public class MySpaceship {

    private Object3D spaceship;

    public Object3D getGlass() {
        return glass;
    }

    private Object3D glass;

    private float xRot = 0;
    private float yRot = 0;
    private int counter = 0;

    Utils utils = new Utils();

//Getters and setters

    public Object3D getSpaceship() {
        return spaceship;
    }

    float getxRot() {
        return xRot;
    }

    void setxRot(float xRot) {
        this.xRot = xRot;
    }

    float getyRot() {
        return yRot;
    }

    void setyRot(float yRot) {
        this.yRot = yRot;
    }

    void createSpaceship(World world) {
        Texture blue = new Texture(64, 64, RGBColor.BLUE);
        TextureManager.getInstance().addTexture("blue", blue);

        glass = utils.loadModel("glas", 50);
        spaceship = utils.loadModel("noglas", 50f);
        spaceship.addChild(glass);
        spaceship.build();
        glass.build();
        glass.setTransparency(10);
        glass.setTransparencyMode(Object3D.TRANSPARENCY_MODE_ADD);
        glass.setTexture("blue");
        spaceship.translate(0, 0, 100);
        world.addObject(glass);
        world.addObject(spaceship);
    }

    void turnLeft() {
        xRot = -0.009f;
    }

    void turnLeftFast() {
        xRot = -0.016f;
    }

    void turnLeftSlow() {
        xRot = -0.005f;
    }

    void turnRight() {
        xRot = 0.009f;
    }

    void turnRightFast() {
        xRot = 0.016f;
    }

    void turnRightSlow() {
        xRot = 0.005f;
    }

    void turnUp() {
        if (counter < 100) {
            yRot = -0.007f;
            counter++;
        } else {
            yRot = 0;
        }
    }

    void turnDown() {
        if (counter > -100) {
            yRot = 0.007f;
            counter--;
        } else {
            yRot = 0;
        }
    }


}
