package com.mikaelquick.billy.space;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;


public class Startmenu extends AppCompatActivity {

    //Textviews & others
    TextView textView, newHighscoreTextview;
    Button newGameButton, highscore, exitButton;
    Typeface starwarsFont;
    boolean isLoaded = false;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Fullscreen
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_startmenu);

        //Initierar
        textView = (TextView) findViewById(R.id.logoStarwars);
        newGameButton = (Button) findViewById(R.id.newGameButton);
        highscore = (Button) findViewById(R.id.highscoreButton);
        exitButton = (Button) findViewById(R.id.exitButton);
        newHighscoreTextview = (TextView) findViewById(R.id.highscoreFound);

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .build();
        mAdView.loadAd(adRequest);

        isLoaded = true;
        starwarsFont = Typeface.createFromAsset(getAssets(), "starwars.ttf");

        highscore.setTypeface(starwarsFont);
        newGameButton.setTypeface(starwarsFont);
        textView.setTypeface(starwarsFont);
        exitButton.setTypeface(starwarsFont);

        newGameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Startmenu.this, MyGame.class);
                startActivity(intent);
            }
        });

        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.exit(0);
            }
        });

        highscore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Startmenu.this, Highscore.class);
                startActivity(intent);
            }
        });

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Startmenu.this, Credits.class);
                startActivity(intent);
            }
        });

    }
}
