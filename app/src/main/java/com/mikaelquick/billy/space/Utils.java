package com.mikaelquick.billy.space;

import android.util.Log;

import com.threed.jpct.Camera;
import com.threed.jpct.Loader;
import com.threed.jpct.Matrix;
import com.threed.jpct.Object3D;
import com.threed.jpct.SimpleVector;
import com.threed.jpct.World;
import java.io.InputStream;

public class Utils {

    protected Object3D loadModel(String name, float scale) {
        InputStream inStream = null;
        try {
            inStream = this.getClass().getClassLoader().getResourceAsStream("res/raw/" + name + ".3ds");
        } catch (Exception e) {
            Log.d("FileError", "Cant find file");
        }
        Object3D[] model = Loader.load3DS(inStream, scale);
        Object3D o3d = new Object3D(0);
        Object3D temp = null;
        for (int i = 0; i < model.length; i++) {
            temp = model[i];
            temp.setCenter(SimpleVector.ORIGIN);
            temp.rotateX((float) (-.5 * Math.PI));
            temp.rotateMesh();
            temp.setRotationMatrix(new Matrix());
            o3d = Object3D.mergeObjects(o3d, temp);
            o3d.build();
        }
        return o3d;
    }

    public static void cameraFollow(Object3D whatToFollow, Camera camera, World world, float yOff, float zOff) {
        SimpleVector oldCamPos = camera.getPosition();
        SimpleVector oldestCamPos = new SimpleVector(oldCamPos);
        oldCamPos.scalarMul(9f);

        SimpleVector carCenter = whatToFollow.getTransformedCenter();
        SimpleVector camPos = new SimpleVector(carCenter);
        SimpleVector zOffset = whatToFollow.getZAxis();
        SimpleVector yOffset = new SimpleVector(0, yOff, 0);
        zOffset.scalarMul(zOff);

        camPos.add(zOffset);
        camPos.add(yOffset);

        camPos.add(oldCamPos);
        camPos.scalarMul(0.1f);

        SimpleVector delta = camPos.calcSub(oldestCamPos);
        float len = delta.length();


        if (len != 0) {

           world.checkCameraCollisionEllipsoid(delta.normalize(), new SimpleVector(0, 0, 0), len, 3);

        }

        camera.lookAt(whatToFollow.getTransformedCenter());
    }


}
